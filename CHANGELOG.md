# Change Log

# Version 14.0-1

	gpg: Signature made Fri 17 Jan 2020 22:26:43 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

Last updated according to www.conorab.com: 2020-01-15 

# Version 12

	gpg: Signature made Sun 05 Jan 2020 13:01:15 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

Last updated according to www.conorab.com:  2020-01-04 

# Version 11

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 9.0

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 8.0

	gpg: Signature made Sun 15 Dec 2019 22:45:01 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 6.2

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 6.1

	gpg: Signature made Sun 15 Dec 2019 22:45:01 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]	

# Version 6

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 5

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

# Version 4

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]
